import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './componentes/home/home.component';
import { NavbarComponent } from './componentes/items/navbar/navbar.component';
import { TarjetasComponent } from './componentes/tarjetas/tarjetas.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


//#region angular material
import { RouterModule } from '@angular/router';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { DialogLoginComponent } from './componentes/items/dialog-login/dialog-login.component';

//#endregion
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    TarjetasComponent,
    DialogLoginComponent,

    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    RouterModule,
    MatInputModule,
    

  ],
  exports:[
    MatFormFieldModule,
    RouterModule,
    MatInputModule,
 

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
