export interface Tienda{
    id:number;
    idUsuario:number;
    nombre:string;
    direccion:string;
    tipoTienda:string;
    horaAbierto:string;
    horaCerrado:string;
    popularidad:string;
    urlLogo:string;
}